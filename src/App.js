import React, { useState, useEffect } from "react";
import "./App.css";
import { makeStyles } from "@material-ui/core/styles";
import {
  Paper,
  TableRow,
  TableHead,
  TableCell,
  TableBody,
  Table,
  Button
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
    marginBottom: theme.spacing(3)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  table: {
    minWidth: 650
  },
  loading: {
    height: 10
  },
  none: {
    display: "none"
  },
  error: {
    padding: 10,
    display: "flex",
    justifyContent: "center"
  },
  running: {
    backgroundColor: 'green'
  },
  pending: {
    backgroundColor: 'yellow'
  },
  stopped: {
    backgroundColor: 'red'
  }
}));

let header = new Headers({
  'Access-Control-Allow-Origin':'*',
  'Content-Type': 'multipart/form-data'
});
let sentData = {
    method: 'GET',
    mode: 'no-cors',
    header: header,
};

const API = {
  instances:
    "https://ceblg52mra.execute-api.eu-central-1.amazonaws.com/liveApi/instances",
  start: ({ id, region }) =>
    `https://ceblg52mra.execute-api.eu-central-1.amazonaws.com/liveApi/startinstance?id=${id}&region=${region}`,
  stop: ({ id, region }) =>
    `https://ceblg52mra.execute-api.eu-central-1.amazonaws.com/liveApi/stopinstance?id=${id}&region=${region}`
};

function App() {
  const classes = useStyles();
  const [table, setTable] = useState([]);


  function _generateRows() {
    return table.map(row => {
      return (
        <TableRow key={row['id:']} className={classes[row.Status === 'stopping' ? 'stopped' : row.Status]}>
          <TableCell align="left">{row.Name}</TableCell>
          <TableCell align="center">{row.Region}</TableCell>
          <TableCell align="center">{row.Status}</TableCell>
          <TableCell align="center">
            <Button
              variant="contained"
              color="primary"
              className={classes.formControl}
              disabled={row.Status === "running" || row.Status === "pending"}
              onClick={changeStatus(row['id:'], row.Region, 'start')}
            >
              Start
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.formControl}
              disabled={row.Status === "stopped" || row.Status === "stopping"}
              onClick={changeStatus(row['id:'], row.Region, 'stop')}
            >
              Stop
            </Button>
          </TableCell>
        </TableRow>
      );
    });
  }

  const fetchData = async () => {
    try {
      const { body } = await fetch(API.instances).then(data => data.json());
      setTable(body);
    } catch (error) {
      console.log(error)
      throw error
    }
  };

  const changeStatus = (id, region, status) => async () => {
    try {
      await fetch(API[status]({id, region}), sentData);
      await fetchData();
    } catch (error) {
      console.log(error)
      throw error
    }
  };

  useEffect(() => {
    fetchData();
    let timer = setInterval(fetchData, 10000)
    return () => {
      clearInterval(timer)
    }
  }, []);

  return (
    <div className="container">
      <div className="table">
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell align="left">Instance Name</TableCell>
                <TableCell align="center">Instance Region</TableCell>
                <TableCell align="center">Instance Status</TableCell>
                <TableCell align="center"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{_generateRows()}</TableBody>
          </Table>
        </Paper>
      </div>
    </div>
  );
}

export default App;
